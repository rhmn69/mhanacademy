package id.co.coffecode.mhanacademyv2.Utility;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

import id.co.coffecode.mhanacademyv2.MainActivity;
import id.co.coffecode.mhanacademyv2.WelcomeActivity;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences sharedPreferences;

    public SharedPreferences.Editor editor;
    public Context context;

    public final  int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "LOGIN";
    private static final String IS_LOGIN = "IS_LOGIN";

    public static final String NAME = "NAME";
    public static final String USERNAME = "USERNAME";

    public SessionManager(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void setLogin(boolean isLoggedIn){
        editor.putBoolean(IS_LOGIN, isLoggedIn);

        //commit changes
        editor.commit();
    }

    public void createSession(String name, String username){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(NAME, name);
        editor.putString(USERNAME, username);
        editor.apply();
    }

    public boolean isLoggin(){
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public void checkLogin(){
        if (!this.isLoggin()){
            Intent intent_welcome = new Intent(context, WelcomeActivity.class);
            context.startActivity(intent_welcome);
            ((MainActivity) context).finish();
        }
    }

    public HashMap<String, String> getUserDetail(){
        HashMap<String, String> user = new HashMap<>();
        user.put(NAME, sharedPreferences.getString(NAME, null));
        user.put(USERNAME, sharedPreferences.getString(USERNAME, null));

        return user;
    }

    public void logout(){
        editor.clear();
        editor.commit();
        Intent intent_welcome = new Intent(context, WelcomeActivity.class);
        context.startActivity(intent_welcome);
    }
}
