package id.co.coffecode.mhanacademyv2.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.co.coffecode.mhanacademyv2.R;

public class AdapterWelcome extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;

    public AdapterWelcome(Context context) {
        this.context = context;
    }

    public String[] welcome_heading = {
            "EAT",
            "SLEEP",
            "CODE"
    };

    public String[] welcome_description = {
            "Lorem Ipsum dolor sit amet, consectur adipiscing, sed do eius tempor",
            "Lorem Ipsum dolor sit amet, consectur adipiscing, sed do eius tempor",
            "Lorem Ipsum dolor sit amet, consectur adipiscing, sed do eius tempor"
    };

    public int[] welcome_image = {
            R.drawable.ic_android_white_100dp,
            R.drawable.ic_android_white_100dp,
            R.drawable.ic_android_white_100dp
    };

    public int[] welcome_bg = {

    };
    @Override
    public int getCount() {
        return welcome_heading.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (LinearLayout) object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_welcome, container, false);

        ImageView imageView_welcome = (ImageView) view.findViewById(R.id.imageView_welcome);
        TextView textView_titleWelcome = (TextView) view.findViewById(R.id.textView_titleWelcome);
        TextView textView_descWelcome = (TextView) view.findViewById(R.id.textView_descWelcome);

        imageView_welcome.setImageResource(welcome_image[position]);
        textView_titleWelcome.setText(welcome_heading[position]);
        textView_descWelcome.setText(welcome_description[position]);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
