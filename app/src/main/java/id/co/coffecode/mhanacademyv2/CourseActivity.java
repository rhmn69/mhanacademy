package id.co.coffecode.mhanacademyv2;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.coffecode.mhanacademyv2.Adapter.AdapterCourse;
import id.co.coffecode.mhanacademyv2.Model.ModelCourse;
import id.co.coffecode.mhanacademyv2.Utility.Common;
import id.co.coffecode.mhanacademyv2.Utility.URLs;
import id.co.coffecode.mhanacademyv2.Utility.Utility;
import id.co.coffecode.mhanacademyv2.Utility.VolleySingleton;

public class CourseActivity extends AppCompatActivity implements DiscreteScrollView.ScrollListener<AdapterCourse.ViewHolder>{

    @BindView(R.id.discreteScrollView_course) DiscreteScrollView scrollView_course;
    @BindView(R.id.progressBar_course) ProgressBar progressBar_course;
    @BindView(R.id.text) TextView textView;

    private ArrayList<ModelCourse> dataCourse = new ArrayList<>();
    private ModelCourse modelCourse;
    private AdapterCourse adapterCourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String ID = intent.getStringExtra("CatalogID");

        scrollView_course.getCurrentItem();
        scrollView_course.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.9f)
                .build());

        loadCourse();
    }

    private void loadCourse() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_COURSE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressBar_course.setVisibility(View.GONE);

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("course");

                            for (int i=0; i < jsonArray.length(); i++){
                                JSONObject data_catalog = jsonArray.getJSONObject(i);

                                if (data_catalog.getString("CatalogID").equals(Common.CatalogID)){
                                    String CourseID = data_catalog.getString("CourseID");
                                    String CatalogID = data_catalog.getString("CatalogID");
                                    String Title = data_catalog.getString("Title");
                                    String Image = data_catalog.getString("Image");
                                    String Progress = data_catalog.getString("Progress");
                                    String Description = data_catalog.getString("Description");
                                    modelCourse = new ModelCourse(CourseID, CatalogID, Title, Image, Progress, Description);
                                    dataCourse.add(modelCourse);
                                }else {
                                    Utility.Toast(getApplicationContext(), "NULL");
                                }
                            }

                            adapterCourse = new AdapterCourse(CourseActivity.this, dataCourse);
                            scrollView_course.setAdapter(adapterCourse);
                            scrollView_course.scrollToPosition(0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.Toast(getApplicationContext(),"Error something going wrong, you should fix it DUDE!!");
            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }


    @Override
    public void onScroll(float scrollPosition, int currentPosition, int newPosition, @Nullable AdapterCourse.ViewHolder currentHolder, @Nullable AdapterCourse.ViewHolder newCurrent) {

    }
}
