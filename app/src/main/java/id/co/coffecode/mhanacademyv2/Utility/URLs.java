package id.co.coffecode.mhanacademyv2.Utility;

public class URLs {
    private static final String ROOT_URL = "http://192.168.100.48/mhanacademy/api/";

    public static final String URL_REGISTER = ROOT_URL + "cuser/add";
    public static final String URL_LOGIN= ROOT_URL + "cuser/auth";
    public static final String URL_CATALOG= ROOT_URL + "ccatalog/all";
    public static final String URL_COURSE = ROOT_URL + "ccourse/all";
    public static final String URL_CONTENT = ROOT_URL + "ccontent/all";
}
