package id.co.coffecode.mhanacademyv2;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.coffecode.mhanacademyv2.Utility.SessionManager;
import id.co.coffecode.mhanacademyv2.Utility.URLs;
import id.co.coffecode.mhanacademyv2.Utility.Utility;
import id.co.coffecode.mhanacademyv2.Utility.VolleySingleton;

public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.editText_username)
    TextView editText_username;
    @BindView(R.id.editText_password) TextView editText_password;
    @BindView(R.id.editText_usernameWrapper)
    TextInputLayout editText_usernameWrapper;
    @BindView(R.id.editText_passwordWrapper) TextInputLayout editText_passwordWrapper;

    public String username;
    public String password;

    public SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        //session manager
        sessionManager = new SessionManager(SignInActivity.this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getWindow().getDecorView().setSystemUiVisibility(Utility.Full_Layout());
    }

    @OnClick(R.id.button_signUp)
    public void signup() {
        Intent intent_signup = new Intent(SignInActivity.this, SignUpActivity.class);
        startActivity(intent_signup);
    }

    @OnClick(R.id.button_login)
    public void login(){
        boolean isValid = true;

        username = editText_username.getText().toString();
        password = editText_password.getText().toString();

        Log.d("SignIN", "login: "+URLs.URL_LOGIN);
        if (TextUtils.isEmpty(username)){
            editText_usernameWrapper.setError("Please enter your username");
            editText_username.requestFocus();
            isValid = false;
        }else {
            editText_usernameWrapper.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(password)){
            editText_passwordWrapper.setError("Please enter your password");
            editText_password.requestFocus();
            isValid = false;
        }else {
            editText_passwordWrapper.setErrorEnabled(false);
        }

        if (isValid){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_LOGIN,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                JSONObject data = jsonObject.getJSONObject("data");
                                Log.d("mhanLogin", "onResponse: "+jsonObject.getString("status"));
                                String status = jsonObject.getString("status");
                                if (status.equals("true")){
                                    Intent intent_home = new Intent(SignInActivity.this, MainActivity.class);
                                    intent_home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                    String name = data.getString("Name");
                                    String email = data.getString("Email");
                                    String username = data.getString("Username");
                                    sessionManager.createSession(name,username);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("Name",name);
                                    bundle.putString("Email",email);
                                    intent_home.putExtras(bundle);
                                    Utility.showProgressDialog(SignInActivity.this);
                                    startActivity(intent_home);
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utility.Toast(SignInActivity.this,"Error : " +error.getMessage());
                    Utility.hideProgressDialog(SignInActivity.this);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Username",username);
                    params.put("Password",password);
                    return params;
                }
            };

            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }
    }
}
