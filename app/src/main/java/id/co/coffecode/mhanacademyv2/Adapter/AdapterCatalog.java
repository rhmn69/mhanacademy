package id.co.coffecode.mhanacademyv2.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.coffecode.mhanacademyv2.CatalogDetailActivity;
import id.co.coffecode.mhanacademyv2.Interface.ItemClickListener;
import id.co.coffecode.mhanacademyv2.Model.ModelCatalog;
import id.co.coffecode.mhanacademyv2.R;
import id.co.coffecode.mhanacademyv2.Utility.Utility;

public class AdapterCatalog extends RecyclerView.Adapter<AdapterCatalog.ViewHolder> {

    private ArrayList<ModelCatalog> dataCatalog;
    private ItemClickListener itemClickListener;
    private Context context;

    public AdapterCatalog(ArrayList<ModelCatalog> modelCatalog, Context context) {
        this.dataCatalog = modelCatalog;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_catalog,viewGroup,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ModelCatalog modelCatalog = dataCatalog.get(i);
        Picasso.with(context).load(modelCatalog.getImage()).fit().centerInside().into(viewHolder.imageView_catalog);
        viewHolder.textView_title.setText(modelCatalog.getTitle());
        viewHolder.textView_subTitle.setText(modelCatalog.getSubTitle());

        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Intent intent_detail_catalog = new Intent(context, CatalogDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("Title", dataCatalog.get(position).getTitle());
                bundle.putString("Description", dataCatalog.get(position).getDescription());
                bundle.putString("Image", dataCatalog.get(position).getImage());
                bundle.putString("CatalogID", dataCatalog.get(position).getCatalogID());
                intent_detail_catalog.putExtras(bundle);
                context.startActivity(intent_detail_catalog);
                Log.d("mhanAdapter", "onClick: "+dataCatalog.get(position).getTitle());
                Log.d("mhanAdapter", "onClick: "+dataCatalog.get(position).getDescription());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataCatalog.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.imageView_catalog) ImageView imageView_catalog;
        @BindView(R.id.textView_title) TextView textView_title;
        @BindView(R.id.textView_subTitle) TextView textView_subTitle;

        private ItemClickListener itemClickListener;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition(),false);
        }
    }
}
