package id.co.coffecode.mhanacademyv2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.coffecode.mhanacademyv2.Adapter.AdapterCatalog;
import id.co.coffecode.mhanacademyv2.Fragment.CatalogFragment;
import id.co.coffecode.mhanacademyv2.Fragment.RankFragment;
import id.co.coffecode.mhanacademyv2.Model.ModelCatalog;
import id.co.coffecode.mhanacademyv2.Utility.BottomNavigationBehavior;
import id.co.coffecode.mhanacademyv2.Utility.SessionManager;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.recyclerView_catalog) RecyclerView recyclerView_catalog;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.bottomNavigation_catalog) BottomNavigationView bottomNavigation_catalog;
    @BindView(R.id.button_logout) Button button_logout;

    private ArrayList<ModelCatalog> dataCatalog=new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private AdapterCatalog adapterCatalog;
    private Bundle getBundle = null;
    private ModelCatalog modelCatalog;

    //init textview from header
    private TextView textView_name;
    private TextView textView_email;
    public SessionManager sessionManager;

    //init variabel local
    private String Name;
    private String Username;
    private String Email;

    //Fragment
    private CatalogFragment catalogFragment;
    private RankFragment rankFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        // init textview from header
        View headerView = navigationView.getHeaderView(0);
        textView_name = headerView.findViewById(R.id.textView_name);
        textView_email = headerView.findViewById(R.id.textView_email);

        // check if logged in
        sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager.isLoggin()){
            button_logout.setVisibility(View.VISIBLE);
            // Load data from session
            HashMap<String, String> data_user = sessionManager.getUserDetail();
            Name = data_user.get(sessionManager.NAME);
            Username = data_user.get(sessionManager.USERNAME);

            // set data to textview from bundle
            textView_name.setText(Name);
            textView_email.setText(Username);
        }else {
            button_logout.setVisibility(View.INVISIBLE);
            // set data to textview from bundle
            textView_name.setText("Welcome");
            textView_email.setText("Guest");
        }

        /*
        *
        * Initial recyclerview catalog
        * */
        recyclerView_catalog.setHasFixedSize(true);
        recyclerView_catalog.setLayoutManager(new LinearLayoutManager(this));
        /*
        * Get Bundle data from signIn activity
        * */

        bottomNavigation_catalog.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_catalog :
                        catalogFragment = new CatalogFragment();
                        setFragment(catalogFragment);
                        break;
                    case R.id.action_ranking :
                        rankFragment = new RankFragment();
                        setFragment(rankFragment);
                        break;
                }
                return true;
            }
        });

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottomNavigation_catalog.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());

//        loadCatalog();
        //init default fragment
        catalogFragment = new CatalogFragment();
        setFragment(catalogFragment);

    }

    @OnClick(R.id.button_logout)
    public void logout(){
        sessionManager.setLogin(false);
        Intent intent_welcome = new Intent(MainActivity.this, WelcomeActivity.class);
        intent_welcome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent_welcome);
        finish();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_search) {
            // Handle the camera action
            Toast.makeText(getApplicationContext(), ""+item.getTitle(), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_catalog) {
            Toast.makeText(getApplicationContext(), ""+item.getTitle(), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_courses) {
            Intent myCourseIntent = new Intent(MainActivity.this,MyCourseActivity.class);
            startActivity(myCourseIntent);
        } else if (id == R.id.nav_about) {
            Intent aboutIntent = new Intent(MainActivity.this,AboutActivity.class);
            startActivity(aboutIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_main,fragment);
        fragmentTransaction.commit();
    }
}
