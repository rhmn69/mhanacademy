package id.co.coffecode.mhanacademyv2.Model;

public class ModelContent {
    private String ContentID, CourseID, CatalogID, Paragraph1, Paragraph2;

    public ModelContent() {
    }

    public ModelContent(String contentID, String courseID, String catalogID, String paragraph1, String paragraph2) {
        ContentID = contentID;
        CourseID = courseID;
        CatalogID = catalogID;
        Paragraph1 = paragraph1;
        Paragraph2 = paragraph2;
    }

    public String getContentID() {
        return ContentID;
    }

    public void setContentID(String contentID) {
        ContentID = contentID;
    }

    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String courseID) {
        CourseID = courseID;
    }

    public String getCatalogID() {
        return CatalogID;
    }

    public void setCatalogID(String catalogID) {
        CatalogID = catalogID;
    }

    public String getParagraph1() {
        return Paragraph1;
    }

    public void setParagraph1(String paragraph1) {
        Paragraph1 = paragraph1;
    }

    public String getParagraph2() {
        return Paragraph2;
    }

    public void setParagraph2(String paragraph2) {
        Paragraph2 = paragraph2;
    }
}
