package id.co.coffecode.mhanacademyv2.Model;

public class ModelUser {
    private String UserID, Name, Email, Username, Password;

    public ModelUser() {
    }

    public ModelUser(String userID, String name, String email, String username, String password) {
        UserID = userID;
        Name = name;
        Email = email;
        Username = username;
        Password = password;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
