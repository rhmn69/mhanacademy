package id.co.coffecode.mhanacademyv2;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import butterknife.BindView;

import butterknife.ButterKnife;

public class AboutActivity extends AppCompatActivity {
    @BindView(R.id.text)
    TextView textView;

    @BindView(R.id.button) Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        new doit().execute();
    }

    public class doit extends AsyncTask<Void, Void, Void>{

        String words;
        @Override
        protected Void doInBackground(Void... voids) {

            try {
                Document document = Jsoup.connect("https://www.google.com/").get();
                words = document.text();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            textView.setText(words);
        }
    }
}
