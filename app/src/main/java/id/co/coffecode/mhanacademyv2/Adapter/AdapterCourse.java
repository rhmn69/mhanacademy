package id.co.coffecode.mhanacademyv2.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.coffecode.mhanacademyv2.ContentElectree.CurrentActivity;
import id.co.coffecode.mhanacademyv2.Interface.ItemClickListener;
import id.co.coffecode.mhanacademyv2.Model.ModelCourse;
import id.co.coffecode.mhanacademyv2.R;
import id.co.coffecode.mhanacademyv2.Utility.Common;
import id.co.coffecode.mhanacademyv2.Utility.Utility;

public class AdapterCourse extends RecyclerView.Adapter<AdapterCourse.ViewHolder> {

    private Context context;
    private ArrayList<ModelCourse> dataCourse;

    public AdapterCourse(Context context, ArrayList<ModelCourse> dataCourse) {
        this.context = context;
        this.dataCourse = dataCourse;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_course,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ModelCourse modelCourse = dataCourse.get(position);
        Picasso.with(context).load(modelCourse.getImage()).into(holder.imageView_course);
        holder.textView_title_course.setText(modelCourse.getTitle());
        holder.textView_progress_course.setText(modelCourse.getProgress());
        holder.textView_description_course.setText(modelCourse.getDescription());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                switch (dataCourse.get(position).getCourseID()){
                    case "1" :
                        Intent intent_current = new Intent(context, CurrentActivity.class);
                        Common.CourseID = dataCourse.get(position).getCourseID();
                        context.startActivity(intent_current);
                        break;
                    case "2" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "3" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "4" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "5" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "6" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "7" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "8" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "9" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "10" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "11" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                    case "12" :
                        Utility.Toast(context,modelCourse.getTitle());
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataCourse.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.imageView_course) ImageView imageView_course;
        @BindView(R.id.textView_title_course) TextView textView_title_course;
        @BindView(R.id.textView_description_course) TextView textView_description_course;
        @BindView(R.id.textView_progress_course) TextView textView_progress_course;

        private ItemClickListener itemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }
    }
}
