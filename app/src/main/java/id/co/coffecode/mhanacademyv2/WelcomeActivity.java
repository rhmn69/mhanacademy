package id.co.coffecode.mhanacademyv2;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.coffecode.mhanacademyv2.Adapter.AdapterWelcome;
import id.co.coffecode.mhanacademyv2.Utility.SessionManager;
import id.co.coffecode.mhanacademyv2.Utility.Utility;
import me.relex.circleindicator.CircleIndicator;

public class WelcomeActivity extends AppCompatActivity {

    @BindView(R.id.viewPager_welcome) ViewPager viewPager_welcome;
    @BindView(R.id.indicator_welcome) CircleIndicator indicator_welcome;

    private AdapterWelcome adapterWelcome;
    public SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

        adapterWelcome = new AdapterWelcome(this);
        viewPager_welcome.setAdapter(adapterWelcome);
        indicator_welcome.setViewPager(viewPager_welcome);

        sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager.isLoggin()){
            Intent intent_main = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(intent_main);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getWindow().getDecorView().setSystemUiVisibility(Utility.Full_Layout());
    }

    @OnClick({R.id.button_home, R.id.button_login})
    public void clickButton(View view) {
        switch (view.getId()){
            case R.id.button_home:
                Intent intent_home = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(intent_home);
                break;
            case R.id.button_login :
                Intent intent_login = new Intent(WelcomeActivity.this, SignInActivity.class);
                startActivity(intent_login);
                break;
        }
    }
}
