package id.co.coffecode.mhanacademyv2.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.coffecode.mhanacademyv2.Adapter.AdapterCatalog;
import id.co.coffecode.mhanacademyv2.Model.ModelCatalog;
import id.co.coffecode.mhanacademyv2.R;
import id.co.coffecode.mhanacademyv2.Utility.URLs;
import id.co.coffecode.mhanacademyv2.Utility.Utility;
import id.co.coffecode.mhanacademyv2.Utility.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class CatalogFragment extends Fragment {

    @BindView(R.id.recyclerView_catalog) RecyclerView recyclerView_catalog;

    private ArrayList<ModelCatalog> dataCatalog;
    private AdapterCatalog adapterCatalog;
    private ModelCatalog modelCatalog;

    public CatalogFragment() {
        // Required empty public constructor
    }

    public static CatalogFragment newInstance(String param1, String param2) {
        CatalogFragment catalogFragment = new CatalogFragment();
        Bundle args = new Bundle();
        catalogFragment.setArguments(args);
        return catalogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this,view);
        dataCatalog = new ArrayList<>();
        adapterCatalog = new AdapterCatalog(dataCatalog, getActivity());

        recyclerView_catalog.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView_catalog.setAdapter(adapterCatalog);

        fetchCatalogItems();
        return view;
    }

    private void fetchCatalogItems() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_CATALOG,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("catalog");
                            for (int i=0; i < jsonArray.length(); i++){
                                JSONObject data_catalog = jsonArray.getJSONObject(i);
                                String CatalogID = data_catalog.getString("CatalogID");
                                String Title = data_catalog.getString("Title");
                                String SubTitle = data_catalog.getString("SubTitle");
                                String Description = data_catalog.getString("Description");
                                String Image = data_catalog.getString("Image");

                                modelCatalog = new ModelCatalog(CatalogID, Title, SubTitle, Description, Image);
                                dataCatalog.add(modelCatalog);
                            }
                            adapterCatalog.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utility.Toast(getActivity(),"Couldn't fetch catalog items please try again!!!");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.Toast(getActivity(),"Error : "+error.getMessage());
            }
        });

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

}
