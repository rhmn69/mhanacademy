package id.co.coffecode.mhanacademyv2.Model;

public class ModelCatalog {
    private String CatalogID, Title, SubTitle, Description, Image;

    public ModelCatalog() {
    }

    public ModelCatalog(String catalogID, String title, String subTitle, String description, String image) {
        CatalogID = catalogID;
        Title = title;
        SubTitle = subTitle;
        Description = description;
        Image = image;
    }

    public String getCatalogID() {
        return CatalogID;
    }

    public void setCatalogID(String catalogID) {
        CatalogID = catalogID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSubTitle() {
        return SubTitle;
    }

    public void setSubTitle(String subTitle) {
        SubTitle = subTitle;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
