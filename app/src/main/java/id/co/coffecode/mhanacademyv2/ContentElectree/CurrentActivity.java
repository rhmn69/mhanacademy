package id.co.coffecode.mhanacademyv2.ContentElectree;

import android.app.ActionBar;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.coffecode.mhanacademyv2.Model.ModelCatalog;
import id.co.coffecode.mhanacademyv2.Model.ModelContent;
import id.co.coffecode.mhanacademyv2.R;
import id.co.coffecode.mhanacademyv2.Utility.Common;
import id.co.coffecode.mhanacademyv2.Utility.URLs;
import id.co.coffecode.mhanacademyv2.Utility.Utility;
import id.co.coffecode.mhanacademyv2.Utility.VolleySingleton;

public class CurrentActivity extends AppCompatActivity {

    @BindView(R.id.paragraph1) TextView paragraph1;
    @BindView(R.id.paragraph2) TextView paragraph2;
    @BindView(R.id.toolbar_current) Toolbar toolbar_current;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;

    private ModelContent modelContent;
    private ArrayList<ModelContent> dataContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current);
        ButterKnife.bind(this);

        getWindow().getDecorView().setSystemUiVisibility(Utility.Full_Layout());

        setSupportActionBar(toolbar_current);

        Log.d("MhanCurrent", "onCreate: "+Common.CourseID);
        fetchCatalogItems();
    }

    private void fetchCatalogItems() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_CONTENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("content");
                            for (int i=0; i<jsonArray.length();i++){
                                JSONObject data_content = jsonArray.getJSONObject(i);
                                if (data_content.getString("CourseID").equals(Common.CourseID)){
                                    String ContentID = data_content.getString("ContentID");
                                    String CourseID = data_content.getString("CourseID");
                                    String CatalogID = data_content.getString("CatalogID");
                                    String Paragraph1 = data_content.getString("Paragraph1");
                                    String Paragraph2 = data_content.getString("Paragraph2");

                                    modelContent = new ModelContent(ContentID, CourseID, CatalogID, Paragraph1, Paragraph2);

                                    paragraph1.setText(Paragraph1);
                                    paragraph2.setText(Paragraph2);
                                }else {
                                    Utility.Toast(CurrentActivity.this,"Data is NULL, please try again!!!");
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utility.Toast(CurrentActivity.this,"Couldn't fetch catalog items please try again!!!");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.Toast(CurrentActivity.this,"Error : "+error.getMessage());
            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getWindow().getDecorView().setSystemUiVisibility(Utility.Full_Layout());
    }
}
