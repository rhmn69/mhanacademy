package id.co.coffecode.mhanacademyv2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.coffecode.mhanacademyv2.Utility.URLs;
import id.co.coffecode.mhanacademyv2.Utility.Utility;
import id.co.coffecode.mhanacademyv2.Utility.VolleySingleton;

import static java.lang.Boolean.TRUE;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.editText_name) EditText editText_name;
    @BindView(R.id.editText_email) EditText editText_email;
    @BindView(R.id.editText_username) EditText editText_username;
    @BindView(R.id.editText_password) EditText editText_password;
    @BindView(R.id.editText_confirmPassword) EditText editText_confirmPassword;

    private String name;
    private String email;
    private String username;
    private String password;
    private String confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getWindow().getDecorView().setSystemUiVisibility(Utility.Full_Layout());
    }

    @OnClick(R.id.button_register)
    public void register_user(){
        name = editText_name.getText().toString();
        email = editText_email.getText().toString();
        username = editText_username.getText().toString();
        password = editText_password.getText().toString();
        confirmPassword = editText_confirmPassword.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            if (jsonObject.getBoolean("status") == TRUE){
                                Utility.Toast(SignUpActivity.this,jsonObject.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utility.Toast(SignUpActivity.this,response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.Toast(SignUpActivity.this,error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Name",name);
                params.put("Email",email);
                params.put("Username",username);
                params.put("Password",password);
                params.put("ConfirmPassword",confirmPassword);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

}
