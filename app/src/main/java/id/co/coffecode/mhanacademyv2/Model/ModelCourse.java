package id.co.coffecode.mhanacademyv2.Model;

public class ModelCourse {
    private String CourseID, CatalogID, Title, Image, Progress, Description;

    public ModelCourse(String courseID, String catalogID, String title, String image, String progress, String description) {
        CourseID = courseID;
        CatalogID = catalogID;
        Title = title;
        Image = image;
        Progress = progress;
        Description = description;
    }

    public String getCourseID() {
        return CourseID;
    }

    public void setCourseID(String courseID) {
        CourseID = courseID;
    }

    public String getCatalogID() {
        return CatalogID;
    }

    public void setCatalogID(String catalogID) {
        CatalogID = catalogID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getProgress() {
        return Progress;
    }

    public void setProgress(String progress) {
        Progress = progress;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
