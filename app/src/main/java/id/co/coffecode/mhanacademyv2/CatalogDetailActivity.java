package id.co.coffecode.mhanacademyv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.coffecode.mhanacademyv2.Utility.Common;
import id.co.coffecode.mhanacademyv2.Utility.SessionManager;
import id.co.coffecode.mhanacademyv2.Utility.Utility;

public class CatalogDetailActivity extends AppCompatActivity {

    @BindView(R.id.textView_descCatalogDetail) TextView descCatalogDetail;
    @BindView(R.id.toolbar_title) Toolbar toolbar_title;
    @BindView(R.id.imageView_catalogDetail) ImageView imageView_catalogDetail;
    @BindView(R.id.progressBar_catalogDesc) ProgressBar progressBar_catalogDesc;

    private String Title;
    private String Description;
    private String Image;
    private Bundle getBundle;

    public SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog_detail);
        ButterKnife.bind(this);

        getBundle = getIntent().getExtras();
        Title = getBundle.getString("Title");
        Description = getBundle.getString("Description");
        Image = getBundle.getString("Image");
        Common.CatalogID = getBundle.getString("CatalogID");

        if (getBundle != null){
            progressBar_catalogDesc.setVisibility(View.GONE);
            descCatalogDetail.setText(Description);
            Picasso.with(getBaseContext()).load(Image).into(imageView_catalogDetail);

            setSupportActionBar(toolbar_title);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(Title);
        }
    }

    @OnClick(R.id.button_enroll)
    public void enroll(){
        sessionManager = new SessionManager(getApplicationContext());
        if (!sessionManager.isLoggin()){
            Utility.Toast(getApplicationContext(),"You are not login, please login!!!");
            Intent intent_welcome = new Intent(getApplicationContext(), WelcomeActivity.class);
            intent_welcome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent_welcome);
        }else {
            Intent intent_course = new Intent(getApplicationContext(), CourseActivity.class);
            intent_course.putExtra("CatalogID", Common.CatalogID);
            startActivity(intent_course);
        }
    }
}
