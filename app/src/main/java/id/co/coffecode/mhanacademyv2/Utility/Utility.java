package id.co.coffecode.mhanacademyv2.Utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

public class Utility {

    private static ProgressDialog progressDialog;

    public static void Toast(Context context, String message){
        Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
    }

    public static int Full_Layout(){
        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        return flags;
    }

    public static void showProgressDialog(Context context){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        if (!progressDialog.isShowing()){
            progressDialog.show();
        }
    }

    public static void hideProgressDialog(Context context){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please Wait...");
        if (progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
}
